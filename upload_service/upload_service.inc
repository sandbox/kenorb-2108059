<?php

/**
 * Save upload attachments to a node.
 *
 * @param $nid
 *   The node ID to use.
 * @param $files
 *   Array of files to attach.
 */
function upload_service_save($nid, $files) {
  $node = node_load($nid);
  
  foreach ($files as $fid => &$file) {
    // Create files that don't exist, update files that do.
    $file['new'] = !isset($node->files[$fid]);
    unset($node->files[$fid]);
  }
  
  // Any remaining files need to be deleted.
  foreach ($node->files as $fid => $file) {
    $files[$fid] = array('remove' => TRUE);
  }
  
  $node->files = $files;
  upload_save($node);
}

/**
 * Check if the user has the permission to save a node.
 *
 * @param $node
 *   Object. The node object.
 * @return
 *   Boolean. TRUE if the user has the permission to save a node.
 */
function upload_service_save_access($nid, $files) {
  $node = node_load($nid);
  return node_access('update', $nid);
}
